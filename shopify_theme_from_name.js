const fs = require('fs');
const https = require('https');

function request(method, host, path, token, body) {
	return new Promise(function (resolve, reject) {
		const headers = {
			'X-Shopify-Access-Token': token,
		}

		if (body) {
			headers['Content-Type'] = 'application/json';
		}

		const req = https.request({
			hostname: host,
			path: path,
			method: method,
			headers: headers,
		}, function (res) {
			var responseBody = '';
			res.on('data', function (data) { responseBody += data });
			res.on('error', reject);
			res.on('end', function () {
				// console.log(res.statusCode, responseBody);
				try {
					resolve(JSON.parse(responseBody));
				} catch (ex) {
					reject(ex);
				}
			});
		});
		req.on('error', reject);
		if (body) {
			req.write(JSON.stringify(body));
		}
		req.end();
	});
}

function writeConfig(params) {
	return fs.writeFileSync('config.yml', JSON.stringify({
		development: params,
	}));
}

(async () => {

	try {
		const token = process.env.PASSWORD;
		const shop = process.env.STORE_URL;
		const themeName = process.env.BITBUCKET_BRANCH;

		const { themes } = await request('GET', shop, '/admin/themes.json', token)

		const existingTheme = themes.filter(theme => theme.name === themeName)[0];

		if (existingTheme) {
			writeConfig({
				password: token,
				store: shop,
				theme_id: existingTheme.id,
			})
		} else {
			const newThemeResponse = await request('POST', shop, '/admin/themes.json', token, {
				theme: {
					name: themeName,
					src: 'https://eric-chan.bitbucket.io/shopify_theme.zip',
					role: 'unpublished',
				}
			})
			writeConfig({
				password: token,
				store: shop,
				theme_id: newThemeResponse.theme.id,
			})
		}
	} catch (ex) {
		console.log(ex);
		process.exit(1);
	}
})();

